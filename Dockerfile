FROM alpine:latest
RUN apk add tar gzip
RUN wget https://github.com/buger/goreplay/releases/download/v1.0.0/gor_1.0.0_x64.tar.gz -O gor.tar.gz
RUN tar -xzf gor.tar.gz
ENTRYPOINT ./gor
